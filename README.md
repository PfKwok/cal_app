
- Create "calculator" module inside app.module.js

- Copy codes into main.controller.js
- Copy codes into calculator.controller.js

- Added following codes into index.html :
    1. ng-app="calculator"
    2. ng-controller="MainCtrl as main"
    3. <script src="/app/main.controller.js"></script>
    4. <script src="/app/calculator.controller.js"></script>

- Copy codes from "/app/app.css" into my "/css/main.css"

- Create views folder as subfolder in client

- Copy codes into calculator.html
- Copy codes into home.html

- Added following codes into index.html :
<div ng-include=" '/views/home.html' " data-ng-show="main.isPageOpen(main.pageType.HOME)"></div>
<div ng-include=" '/views/calculator.html' " ng-show="main.isPageOpen(main.pageType.CALCULATOR)"></div>